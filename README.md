# Pell Extremely Lean Language

Pell is a tiny but practical language that evolved from [Cell](//gitlab.com/cell_lang/cell), which is a minimally simple language.

For now, this repo is almost identical to Cell's but it is free to evolve (and gain more code for useful features) if there are interested contributors.

Here is an example program:

<!-- include "examples/example1.cell" -->
```
square = {:(x) x * x;};

num1 = 3;
num2 = square( num1 );

if( equals( num1, num2 ),
    {
        print( "num1 equals num2." );
    },
    {
        print( "num1 does not equal num2." );
    }
);
```
<!-- end_include -->

This prints:

<!-- include "examples/example1.output.txt" -->
```
num1 does not equal num2.
```
<!-- end_include -->

## Install

```
sudo apt-get install python3
git clone https://gitlab.com/cell_lang/pell.git
cd pell
./cell                  # - to run the interactive environment
./cell filename.cell    # - to run a program
make                    # - to run all the tests
```

## Design Principles

Pell is designed to be very small: it should have as few features as possible
while still being useful.  For example, the fact that language features
such as `if` are provided as functions instead of special keywords means there
is little code needed to write the language, and little learning needed to
understand it.

Pell code should run as fast as possible, where this does not conflict with the
simplicity of the language to use and understand.

Pell's standard library should be "batteries included" - everything you might
want to do with Pell is a candidate for inclusion.  Standard library code
should be opinionated, and make complex tasks easy, as opposed to being
over-generalised.  Where there are several opinionated ways of approaching a
problem, multiple different standard library modules may be provided.

## Features

Pell has:

* Numbers (floating-point)
* Strings
* Functions
* A special "None" value
* Comments

That's about it.

## Interacting with Pell

Pell has an interactive environment ("REPL") which can be launched by running
the Pell program with no arguments:

<!-- include "examples/www.cellsession" -->
```
>>> 137 + 349;
486
>>> 5/10;
0.5
>>> 21 + 35 + 12 + 7;
75
>>> if(equals(0, 1), {"illogical";}, {"logical";});
'logical'
```
<!-- end_include -->

## How to write Pell programs

See the explanation of [Pell Syntax](syntax.md), and [The if function](if.md).

## Building a language from functions

Pell does not provide special syntax for things like lists, maps and
objects, but they can be built up using the features of Cell functions.

The Pell library contains functions like `pair` that makes a simple data
structure from two values (more explanation at
[Data Structures](data_structures.md) ).

You can also build things that behave like objects in languages like Java and
C++ out of functions.  There is more explanation at: [Objects](objects.md).

## How to write a programming language

To find out more about how to write a programming language, have a look at
[Cell](https://gitlab.com/cell_lang/cell), which is the minimally-simple
language that originated Pell.
